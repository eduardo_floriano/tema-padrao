<!DOCTYPE html> <!-- Html 5 -->

<html <?php language_attributes() ?> >
    <head>
        <meta charset="<?php bloginfo('charset') ?>">        
        <title><?php wp_title('-', true, 'right'); bloginfo(); ?></title>

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/bootstrap-theme.min.css" />
        <script src="<?php bloginfo('template_url') ?>/js/bootstrap.min.js"></script>
        
        <!-- FlexSlider -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/css/flexslider.css" />
        <script src="<?php bloginfo('template_url') ?>/js/jquery.flexslider.js"></script>
        <script src="<?php bloginfo('template_url') ?>/js/jquery.flexslider-min.js"></script>
        
        <!-- FontsAwsome -->
        <link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/font-awesome.min.css">
        
        <!-- Css e Js do Tema -->
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url') ?>/style.css" />
        <script language="javascript" src="<?php bloginfo('template_url') ?>/js/functions.js"></script>

<?php wp_head(); ?>
    </head>

    <body>
        
        
        <div class="row">
            
            <div class="logo-panel">
                
                
            </div>
            
            
        </div>
        
        <div class="row">
            
                <!-- Fixed navbar -->
            <div class="navbar navbar-inverse" role="navigation">
              <div class="container">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">Nome do Site</a>
                </div>
                <div class="navbar-collapse collapse">
                  <ul class="nav navbar-nav">
                    <li class="active"><a href="#">INICIO</a></li>
                    <li><a href="#about">SOBRE</a></li>
                    <li><a href="#contact">ACOMPANHANTES</a></li>
                    <li><a href="#contact">CADASTRO</a></li>
                    <li><a href="#contact">CONTATO</a></li>
                    
                  </ul>
                </div><!--/.nav-collapse -->
              </div>
            </div>

            
        </div>